<%@ include file="/WEB-INF/layouts/include.jsp"%>

<orly-table loaddataoncreate url="<%=request.getContextPath()%>/movies/getmovies"  id="myTable">	 
	<orly-column field="action" label="action" class="">
		<div slot="cell">
			
			<%-- <a href="\${`<c:url value="/movies/updateMovies" />`}"> --%>
			<orly-icon color="cornflowerblue" name='edit' id="\${`e_\${model.title}`}"></orly-icon>
			<!-- </a> -->
			
			<a href="\${`<c:url value="/movies/delete?id=\${model.title} " />`}">
				<orly-icon color="red" name='x' id="\${`d_\${model.title}`}"></orly-icon>
			</a>
			
		</div>
	</orly-column> 
	<orly-column field="title" label="Title">
	</orly-column> 
	<orly-column field="releaseDate" label="Release Date">
	</orly-column> 
	<orly-column field="boxOffice" label="Box Office">
	</orly-column> 
	<orly-column field="rating" label="Rating">
	</orly-column> 
</orly-table>


<div class="row d-none" id="addEditForm">
<h1>Add New Movie</h1>
    <div class="col-sm-12">
	<div>${message}</div>
	<form method="post" action="<c:url value='/movies/movieTitles'/>" is="orly-form">
	    <div class="col-sm-4 form-group">
	        <label for="movieTitle">Movie Title</label>
		<orly-input id="movieTitle" name="movieTitle" placeholder="Movie Title"></orly-input>
	    </div>
	    <div class="col-sm-4 form-group">
	        <label for="releaseDate">Release Date</label>
		<orly-input id="releaseDate" name="releaseDate" placeholder="Release Date"></orly-input>
	    </div>
	    <div class="col-sm-4 form-group">
	        <label for="boxOffice">Box Office</label>
		<orly-input id="boxOffice" name="boxOffice" placeholder="Box Office"></orly-input>
	    </div>
	    <div class="col-sm-4 form-group">
	        <label for="rating">Rating</label>
		<orly-input id="rating" name="rating" placeholder="rating"></orly-input>
	    </div>
	    
        <div class="row">
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnSubmit" type="button" class="btn btn-primary w-100 mb-1">Submit</button>
				</div>
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnReset" type="button" class="btn btn-danger w-100 mb-1">Reset</button>
				</div>
				<div class="col-12 col-sm-4 col-md-2">
					<button id="btnCancel" type="button" class="btn btn-warning w-100 mb-1">Cancel</button>		
				</div>
			</div>
        
	</form>
</div>
</div>

<script>
orly.ready.then(()=>{
    var table = orly.qid("myTable");
    table.addEventListener("click", (e)=>{
        var item = e.target;
        var myForm = orly.qid("addEditForm");
        var myTable = orly.qid("myTable");
        if (e.target.tagName == "ORLY-ICON"){
			let targetId = e.target.id;
			if (targetId.startsWith("e_")){
				show("addEditForm");
				hide("myTable");
			}
		}
    });
    
    function show(id) {
    	//(orly.qid(id).classList.contains("d-none")) ? orly.qid(id).classList.remove("d-none") : null ;
    	orly.qid(id).classList.remove("d-none");
    }
    	
    function hide(id) {
    	//(!orly.qid(id).classList.contains("d-none")) ? orly.qid(id).classList.add("d-none") : null ;
    	orly.qid(id).classList.add("d-none");
    }

});
</script>