package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Movie;
import com.oreillyauto.service.MoviesService;

@Controller
public class MoviesController {
    
    @Autowired
    private MoviesService moviesService;

    @GetMapping(value="/movies")
    public String getmovies(Model model, String id) {
        List<Movie> movieList = moviesService.getMovies();
        model.addAttribute("movieList", movieList);
        
//        for (Movie movie : movieList) {
//            System.out.println(movie.getTitle());
//            model.addAttribute("movieTitle", movie.getTitle());
//        }
        return "movies";
    }
    
    @ResponseBody
    @GetMapping(value="/movies/getmovies")
    public List<Movie> getMoviesData() {
        List<Movie> list = moviesService.getMovies();
        return list;
    }
    
    @GetMapping(value="/movies/delete")
    public String deletemovies(Model model, String id) {
        moviesService.deleteById(id);
        return "redirect:/movies";
    }
    
    /*    @GetMapping(value="/movies/updateMovies")
    public String updateMovies(Model model, String id) {
        return "/movies/updateMovies";
    }*/
    // We will likely want to change this to a post mapping, I have handled the form/table change with JS
    // Now we need to handle the form submission with Java.
    
    @PostMapping(value= {"/movies/movieTitles"})
    public String updateMoviePost(Model model, String title, String releaseDate, Integer boxOffice, String rating) {
        // we should get values and edit
        return "redirect:/movies";
    }
    
//    @ResponseBody
//    @PostMapping(value= {"/movies/save"})
//    public String saveMoviePost(@RequestBody Movie movie) {
//        System.out.println(movie);
//        return "redirect:/movies";
//    }
    
    
    /*@ResponseBody
    @PostMapping(value = "/addressBook/save")
    public Message saveAddress(@RequestBody Address address) {
        Message message = new Message();
        try {
            addressBookService.saveAddress(address);
            message.setMessageType("success");
            message.setMessage("Successfully Saved!!!! :)");
            return message;
        } catch (Exception e) {
            message.setMessageType("danger");
            message.setMessage("Not cool.....");
            return message;
        }
    }*/

}