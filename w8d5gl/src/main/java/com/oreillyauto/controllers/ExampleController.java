package com.oreillyauto.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ExampleController {

    @GetMapping(path="/example")
    public String getExample(HttpServletResponse resp) {
        resp.setStatus(403);
        return "example";
    }
    
}
