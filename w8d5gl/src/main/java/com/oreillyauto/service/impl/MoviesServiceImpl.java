package com.oreillyauto.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oreillyauto.dao.MoviesRepository;
import com.oreillyauto.domain.Movie;
import com.oreillyauto.service.MoviesService;

@Service
public class MoviesServiceImpl implements MoviesService{

    @Autowired
    MoviesRepository moviesRepo;
    
    @Override
    public List<Movie> getMovies() {
        
        return moviesRepo.getMovies();
    }
    
    // be sure to add @transactional when we update the database.
    // .delete() and .save() do not need to be reflected in the repository - crud repo handles for us
    @Transactional
    @Override
    public void deleteMovieByTitle(Movie movie) {
        moviesRepo.delete(movie);
    }

    @Override
    public void deleteById(String id) {
        // TODO Auto-generated method stub
        moviesRepo.deleteById(id);
    }

    @Override
    public void updateMovieById(String id) {
        // TODO Auto-generated method stub
        
    }
}