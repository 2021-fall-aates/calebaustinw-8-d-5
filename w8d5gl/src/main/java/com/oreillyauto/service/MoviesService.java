package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Movie;

public interface MoviesService {
    public List<Movie> getMovies();
    public void updateMovieById(String id);
    public void deleteMovieByTitle(Movie movie);
    public void deleteById(String id);
}