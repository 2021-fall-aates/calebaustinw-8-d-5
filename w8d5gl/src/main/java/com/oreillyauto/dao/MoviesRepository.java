package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.MoviesRepositoryCustom;
import com.oreillyauto.domain.Movie;

public interface MoviesRepository extends CrudRepository<Movie, String>, MoviesRepositoryCustom{

}
