package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Movie;

public interface MoviesRepositoryCustom {
    public List<Movie> getMovies();
}
