package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.MoviesRepositoryCustom;
import com.oreillyauto.domain.Movie;
import com.oreillyauto.domain.QMovie;

@Repository
public class MoviesRepositoryImpl extends QuerydslRepositorySupport implements MoviesRepositoryCustom {
    
    QMovie moviesTable = QMovie.movie;
    
    public MoviesRepositoryImpl() {
        super(Movie.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Movie> getMovies() {
        
        List<Movie> movieList = (List<Movie>) (Object) getQuerydsl().createQuery()
                .from(moviesTable)
                .fetch();
        return movieList;
    }
    
}
