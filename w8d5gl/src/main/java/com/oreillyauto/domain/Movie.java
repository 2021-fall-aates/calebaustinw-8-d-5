package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "MOVIES")
public class Movie implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Movie() {
        
    }
    
    public Movie(String title, String releaseDate, Integer boxOffice, String rating) {
        super();
        this.title = title;
        this.releaseDate = releaseDate;
        this.boxOffice = boxOffice;
        this.rating = rating;
    }

    @Id
    @Column(name = "title", columnDefinition = "VARCHAR(64)")
    private String title;
    
    @Column(name="release_date", columnDefinition = "TIMESTAMP")
    private String releaseDate;
    
    @Column(name="box_office", columnDefinition = "INTEGER")
    private Integer boxOffice;
    
    @Column(name="rating", columnDefinition = "VARCHAR(5)")
    private String rating;

    public String getTitle() {
        return title;
    }
    
    @Transient
    private String formattedTitle;

    public String getFormattedTitle() {
        if (title != null && title.length() > 0) {
            title = title.replaceAll(" ", "|");
            return title;
        }
        return (title == null) ? null : "";
    }

    public void setFormattedTitle(String formattedTitle) {
        this.formattedTitle = formattedTitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getBoxOffice() {
        return boxOffice;
    }

    public void setBoxOffice(Integer boxOffice) {
        this.boxOffice = boxOffice;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Movie [title=" + title + ", releaseDate=" + releaseDate + ", boxOffice=" + boxOffice + ", rating=" + rating + "]";
    }
    
}